function show() {

};

show();


$('.container-1').slick({
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    adaptiveHeight: true,
    slidesToShow: 1,
    breakpoint: 1024,
    infinite: true,
    dots: true,
    cssEase: 'linear'
});

$('a[href*="#"]')
   
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
       
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {
            
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
           
            if (target.length) {
                
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top - 50
                }, 1000, function() {
                   
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { 
                        return false;
                    } else {
                        $target.attr('tabindex', '-1'); 
                        $target.focus(); 
                    };
                });
            }
        }
    });

$('body').append('<a href="#" id="go-top"</a>');


$(function() {
    $.fn.scrollToTop = function() {
        $(this).hide().removeAttr("href");
        if ($(window).scrollTop() >= "250") $(this).fadeIn("slow")
        var scrollDiv = $(this);
        $(window).scroll(function() {
            if ($(window).scrollTop() <= "250") $(scrollDiv).fadeOut("slow")
            else $(scrollDiv).fadeIn("slow")
        });
        $(this).click(function() {
            $("html, body").animate({ scrollTop: 0 }, "slow")
        })
    }
});

$(function() {
    $("#go-top").scrollToTop();
});

var bg_Offset = 0;

function scroll_bg() {
    bg_Offset = bg_Offset + 1;
    $(".moving-line").css("backgroundPosition", "0px " + bg_Offset + "px");
}

$(document).ready(function() { setInterval("scroll_bg()", 20); });


