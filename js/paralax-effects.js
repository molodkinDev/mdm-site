function castParallax() {

	var opThresh = 350;
	var opFactor = 750;

	window.addEventListener("scroll", function(event){

		var top = this.pageYOffset;

		var layersHeader = document.getElementsByClassName("parallax-header");
		var layerHeader, speedHeader, yPosHeader;
		for (var i = 0; i < layersHeader.length; i++) {
			layerHeader = layersHeader[i];
			speedHeader = layerHeader.getAttribute('data-speed');
			var yPosHeader = -(top * speedHeader / 100);
			layerHeader.setAttribute('style', 'transform: translate3d(0px, ' + yPosHeader + 'px, 0px)');
		}
		
		var layersSection4 = document.getElementsByClassName("parallax-section4");
		var layerSection4, speedSection4, yPosSection4;
		for (var i = 0; i < layersSection4.length; i++) {
			layerSection4 = layersSection4[i];
			speedSection4 = layerSection4.getAttribute('data-speed');			
			
			 if ($( document ).width() <= 768 ) { 
			var yPosSection4 = -((top - 2400)* speedSection4 / 100);
			layerSection4.setAttribute('style', 'transform: translate3d(0px, ' + yPosSection4 + 'px, 0px)');
			}
			 if ($( document ).width() > 768 && $( document ).width() < 1920 ) { 
			var yPosSection4 = -((top - 2250)* speedSection4 / 100);
			layerSection4.setAttribute('style', 'transform: translate3d(0px, ' + yPosSection4 + 'px, 0px)');
			}			 
			 
			 if ($( document ).width() >= 1920 ) { 
			var yPosSection4 = -((top - 1900)* speedSection4 / 100);
			layerSection4.setAttribute('style', 'transform: translate3d(0px, ' + yPosSection4 + 'px, 0px)');
			}
		}
	});


}

function dispelParallax() {
	$("#nonparallax-header").css('display','block'); 
	//$("#nonparallax").css('display','block'); 
	$("#parallax-header").css('display','none');	
	
	//$("#nonparallax-section4").css('display','block'); 	
	//$("#parallax-section4").css('display','none');
}

function castSmoothScroll() {
	$.srSmoothscroll({
		step: 80,
		speed: 300,
		ease: 'linear'
	});
}



function startSite() {

	var platform = navigator.platform.toLowerCase();
	var userAgent = navigator.userAgent.toLowerCase();

	if ( platform.indexOf('ipad') != -1  ||  platform.indexOf('iphone') != -1 ) 
	{
		dispelParallax();
	}
	
	else if (platform.indexOf('win32') != -1 || platform.indexOf('linux') != -1)
	{
		castParallax();					
		// if ($.browser.webkit)
		// {
		// 	castSmoothScroll();
		// }
	}
	
	else
	{
		castParallax();
	}

}

document.body.onload = startSite();